app = function () {

    var curImg = null;
    var highZ = 550;

    var showDesc = function (event) {
        descs = event.currentTarget.children;
        for (var i = 0; i < descs.length; i++) {
            if (descs[i].attributes != null) {
                if ((descs[i].attributes[0].nodeValue == "headline") || (descs[i].attributes[0].nodeValue == "description")) {
                    descs[i].style.visibility = "visible";
                    descs[i].style.visibility = "visible";
                }
            }
        }
    }

    var hideDesc = function (event) {
        descs = event.currentTarget.children;
        for (var i = 0; i < descs.length; i++) {
            if (descs[i].attributes != null) {
                if ((descs[i].attributes[0].nodeValue == "headline") || (descs[i].attributes[0].nodeValue == "description")) {
                    descs[i].style.visibility = "hidden";
                    descs[i].style.visibility = "hidden";
                }
            }
        }
    }

    var swapDepth = function (img) {
        img.style.zIndex = highZ;
        if (curImg != null) {
            curImg.style.zIndex = highZ - 1;
        }
        curImg = img;
        highZ++;
    }

    var placeImages = function (nodes, holder) {
        var highest = 0;
        var widest = 0;

        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].width > widest){
                widest = nodes[i].width;
            }
            if (nodes[i].height + nodes[i].offsetTop > highest){
                highest = nodes[i].height + nodes[i].offsetTop;
            }
            DragHandler.attach(nodes[i]);
        }
 
        holder.style.height = highest + "px";

    }

    var DragHandler = {
        // private property.
        _oElem: null,
        // public method. Attach drag handler to an element.
        attach: function (oElem) {
            oElem.onmousedown = DragHandler._dragBegin;

            // callbacks
            oElem.dragBegin = new Function();
            oElem.drag = new Function();
            oElem.dragEnd = new Function();

            return oElem;
        },

        // private method. Begin drag process.
        _dragBegin: function (e) {

            swapDepth(this);

            var oElem = DragHandler._oElem = this;

            if (isNaN(parseInt(oElem.style.left))) {
                oElem.style.left = '0px';
            }
            if (isNaN(parseInt(oElem.style.top))) {
                oElem.style.top = '0px';
            }

            var x = parseInt(oElem.style.left);
            var y = parseInt(oElem.style.top);

            e = e ? e : window.event;
            oElem.mouseX = e.clientX;
            oElem.mouseY = e.clientY;

            oElem.dragBegin(oElem, x, y);

            document.onmousemove = DragHandler._drag;
            document.onmouseup = DragHandler._dragEnd;
            return false;
        },


        // private method. Drag (move) element.
        _drag: function (e) {
            var oElem = DragHandler._oElem;

            var x = parseInt(oElem.style.left);
            var y = parseInt(oElem.style.top);

            e = e ? e : window.event;
            oElem.style.left = x + (e.clientX - oElem.mouseX) + 'px';
            oElem.style.top = y + (e.clientY - oElem.mouseY) + 'px';

            oElem.mouseX = e.clientX;
            oElem.mouseY = e.clientY;

            oElem.drag(oElem, x, y);

            return false;
        },


        // private method. Stop drag process.
        _dragEnd: function () {
            var oElem = DragHandler._oElem;

            var x = parseInt(oElem.style.left);
            var y = parseInt(oElem.style.top);

            oElem.dragEnd(oElem, x, y);

            document.onmousemove = null;
            document.onmouseup = null;
            DragHandler._oElem = null;
        }

    }

    var holders = document.getElementsByName("holder");

    for (var i = 0; i < holders.length; i++) {
        placeImages(holders[i].children, holders[i]);
    }

    var projects = document.getElementsByClassName('project');
    for (var i = 0; i < projects.length; i++) {

        projects[i].addEventListener('mouseover', showDesc)
        projects[i].addEventListener('mouseout', hideDesc)
    }
}